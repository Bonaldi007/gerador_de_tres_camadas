﻿namespace Gerador_de_tres_Camadas
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Rb_Entidade = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.bt_gerar = new System.Windows.Forms.Button();
            this.rb_campos = new System.Windows.Forms.RadioButton();
            this.rb_inverte = new System.Windows.Forms.Button();
            this.rb_select = new System.Windows.Forms.RadioButton();
            this.lb_aspas_simples = new System.Windows.Forms.Label();
            this.lb_aspas_duplas = new System.Windows.Forms.Label();
            this.lb_tabela = new System.Windows.Forms.Label();
            this.txt_tabela = new System.Windows.Forms.TextBox();
            this.rb_Update = new System.Windows.Forms.RadioButton();
            this.rb_insert = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(13, 13);
            this.textBox1.Multiline = true;
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(1326, 174);
            this.textBox1.TabIndex = 0;
            this.textBox1.TabIndexChanged += new System.EventHandler(this.textBox1_TextChanged);
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // Rb_Entidade
            // 
            this.Rb_Entidade.AutoSize = true;
            this.Rb_Entidade.Location = new System.Drawing.Point(6, 35);
            this.Rb_Entidade.Name = "Rb_Entidade";
            this.Rb_Entidade.Size = new System.Drawing.Size(98, 24);
            this.Rb_Entidade.TabIndex = 1;
            this.Rb_Entidade.TabStop = true;
            this.Rb_Entidade.Text = "Entidade";
            this.Rb_Entidade.UseVisualStyleBackColor = true;
            this.Rb_Entidade.CheckedChanged += new System.EventHandler(this.Rb_Entidade_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.txt_tabela);
            this.groupBox1.Controls.Add(this.lb_tabela);
            this.groupBox1.Controls.Add(this.rb_insert);
            this.groupBox1.Controls.Add(this.rb_Update);
            this.groupBox1.Controls.Add(this.rb_select);
            this.groupBox1.Controls.Add(this.rb_campos);
            this.groupBox1.Controls.Add(this.Rb_Entidade);
            this.groupBox1.Location = new System.Drawing.Point(13, 193);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1327, 80);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Gerar";
            // 
            // richTextBox1
            // 
            this.richTextBox1.Location = new System.Drawing.Point(13, 368);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.Size = new System.Drawing.Size(1326, 387);
            this.richTextBox1.TabIndex = 4;
            this.richTextBox1.Text = "";
            // 
            // bt_gerar
            // 
            this.bt_gerar.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.bt_gerar.Location = new System.Drawing.Point(12, 315);
            this.bt_gerar.Name = "bt_gerar";
            this.bt_gerar.Size = new System.Drawing.Size(146, 47);
            this.bt_gerar.TabIndex = 2;
            this.bt_gerar.Text = "Gerar";
            this.bt_gerar.UseVisualStyleBackColor = false;
            this.bt_gerar.TabIndexChanged += new System.EventHandler(this.bt_gerar_Click);
            this.bt_gerar.TextChanged += new System.EventHandler(this.bt_gerar_Click);
            this.bt_gerar.Click += new System.EventHandler(this.bt_gerar_Click);
            // 
            // rb_campos
            // 
            this.rb_campos.AutoSize = true;
            this.rb_campos.Location = new System.Drawing.Point(110, 35);
            this.rb_campos.Name = "rb_campos";
            this.rb_campos.Size = new System.Drawing.Size(93, 24);
            this.rb_campos.TabIndex = 2;
            this.rb_campos.TabStop = true;
            this.rb_campos.Text = "Campos";
            this.rb_campos.UseVisualStyleBackColor = true;
            this.rb_campos.CheckedChanged += new System.EventHandler(this.rb_campos_CheckedChanged);
            // 
            // rb_inverte
            // 
            this.rb_inverte.BackColor = System.Drawing.SystemColors.Highlight;
            this.rb_inverte.Location = new System.Drawing.Point(611, 326);
            this.rb_inverte.Name = "rb_inverte";
            this.rb_inverte.Size = new System.Drawing.Size(146, 32);
            this.rb_inverte.TabIndex = 5;
            this.rb_inverte.Text = "Inverter";
            this.rb_inverte.UseVisualStyleBackColor = false;
            this.rb_inverte.Visible = false;
            this.rb_inverte.Click += new System.EventHandler(this.rb_inverte_Click);
            // 
            // rb_select
            // 
            this.rb_select.AutoSize = true;
            this.rb_select.Location = new System.Drawing.Point(290, 35);
            this.rb_select.Name = "rb_select";
            this.rb_select.Size = new System.Drawing.Size(79, 24);
            this.rb_select.TabIndex = 3;
            this.rb_select.TabStop = true;
            this.rb_select.Text = "Select";
            this.rb_select.UseVisualStyleBackColor = true;
            this.rb_select.CheckedChanged += new System.EventHandler(this.rb_select_CheckedChanged);
            // 
            // lb_aspas_simples
            // 
            this.lb_aspas_simples.AutoSize = true;
            this.lb_aspas_simples.Location = new System.Drawing.Point(245, 338);
            this.lb_aspas_simples.Name = "lb_aspas_simples";
            this.lb_aspas_simples.Size = new System.Drawing.Size(12, 20);
            this.lb_aspas_simples.TabIndex = 6;
            this.lb_aspas_simples.Text = "\'";
            this.lb_aspas_simples.Visible = false;
            // 
            // lb_aspas_duplas
            // 
            this.lb_aspas_duplas.AutoSize = true;
            this.lb_aspas_duplas.Location = new System.Drawing.Point(295, 338);
            this.lb_aspas_duplas.Name = "lb_aspas_duplas";
            this.lb_aspas_duplas.Size = new System.Drawing.Size(15, 20);
            this.lb_aspas_duplas.TabIndex = 7;
            this.lb_aspas_duplas.Text = "\"";
            this.lb_aspas_duplas.Visible = false;
            // 
            // lb_tabela
            // 
            this.lb_tabela.AutoSize = true;
            this.lb_tabela.Location = new System.Drawing.Point(502, 39);
            this.lb_tabela.Name = "lb_tabela";
            this.lb_tabela.Size = new System.Drawing.Size(74, 20);
            this.lb_tabela.TabIndex = 8;
            this.lb_tabela.Text = "Tabela = ";
            this.lb_tabela.Visible = false;
            // 
            // txt_tabela
            // 
            this.txt_tabela.Location = new System.Drawing.Point(582, 33);
            this.txt_tabela.Name = "txt_tabela";
            this.txt_tabela.Size = new System.Drawing.Size(538, 26);
            this.txt_tabela.TabIndex = 9;
            this.txt_tabela.Visible = false;
            // 
            // rb_Update
            // 
            this.rb_Update.AutoSize = true;
            this.rb_Update.Location = new System.Drawing.Point(375, 35);
            this.rb_Update.Name = "rb_Update";
            this.rb_Update.Size = new System.Drawing.Size(87, 24);
            this.rb_Update.TabIndex = 4;
            this.rb_Update.TabStop = true;
            this.rb_Update.Text = "Update";
            this.rb_Update.UseVisualStyleBackColor = true;
            this.rb_Update.CheckedChanged += new System.EventHandler(this.rb_Update_CheckedChanged);
            // 
            // rb_insert
            // 
            this.rb_insert.AutoSize = true;
            this.rb_insert.Location = new System.Drawing.Point(209, 35);
            this.rb_insert.Name = "rb_insert";
            this.rb_insert.Size = new System.Drawing.Size(75, 24);
            this.rb_insert.TabIndex = 5;
            this.rb_insert.TabStop = true;
            this.rb_insert.Text = "Insert";
            this.rb_insert.UseVisualStyleBackColor = true;
            this.rb_insert.CheckedChanged += new System.EventHandler(this.rb_insert_CheckedChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(1353, 774);
            this.Controls.Add(this.lb_aspas_duplas);
            this.Controls.Add(this.lb_aspas_simples);
            this.Controls.Add(this.rb_inverte);
            this.Controls.Add(this.bt_gerar);
            this.Controls.Add(this.richTextBox1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.textBox1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Gerador de Campos";
            this.TransparencyKey = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(0)))));
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.RadioButton Rb_Entidade;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.Button bt_gerar;
        private System.Windows.Forms.RadioButton rb_campos;
        private System.Windows.Forms.Button rb_inverte;
        private System.Windows.Forms.RadioButton rb_select;
        private System.Windows.Forms.Label lb_aspas_simples;
        private System.Windows.Forms.Label lb_aspas_duplas;
        private System.Windows.Forms.Label lb_tabela;
        private System.Windows.Forms.TextBox txt_tabela;
        private System.Windows.Forms.RadioButton rb_Update;
        private System.Windows.Forms.RadioButton rb_insert;
    }
}

