﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gerador_de_tres_Camadas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        private void Rb_Entidade_CheckedChanged(object sender, EventArgs e)
        {
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            textBox1.Text = textBox1.Text.Replace("[", "").Replace("]", "").Replace("\n", "").Replace(" ", "").Replace("\r", "");
        }

        private void bt_gerar_Click(object sender, EventArgs e)
        {
            string[] nomes = textBox1.Text.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            string Campo = "";

            if (rb_insert.Checked == true || rb_select.Checked == true || rb_Update.Checked == true)
            {
                if (txt_tabela.Text == "")
                {
                    MessageBox.Show("Digite o nome da Tabela");
                }
                else
                {
                    if (rb_select.Checked == true)
                    {
                        richTextBox1.Text = "";
                        richTextBox1.Text = "string query = @" + lb_aspas_duplas.Text + "select " + textBox1.Text + " from " + txt_tabela.Text + " where " + lb_aspas_duplas.Text + ";\n\n";
                        foreach (string list in nomes)
                        {
                            Campo = list;
                            Campo = "Entidade." + Campo + " = conexao.RecordSet[" + lb_aspas_duplas.Text + Campo + lb_aspas_duplas.Text + "].ToString();\n";
                            richTextBox1.Text += Campo;
                        }
                    }

                    if (rb_Update.Checked == true)
                    {
                        string gerarCampos = "";
                        richTextBox1.Text = "";

                        foreach (string list in nomes)
                        {
                            Campo = list;
                            Campo = Campo + " = " + lb_aspas_duplas.Text + " + gerais.Retorno(Entidade." + Campo + ") + " + lb_aspas_duplas.Text + ", ";
                            gerarCampos += Campo;
                        }
                        gerarCampos = gerarCampos.Remove(gerarCampos.Length - 2);

                        richTextBox1.Text = "string query = @" + lb_aspas_duplas.Text + "update " + txt_tabela.Text + " set " + gerarCampos + " where " + lb_aspas_duplas.Text + ";";
                    }

                    if (rb_insert.Checked == true)
                    {
                        string gerarCampos = "";
                        richTextBox1.Text = "";

                        foreach (string list in nomes)
                        {
                            Campo = list;
                            Campo = lb_aspas_duplas.Text + " + gerais.Retorno(Entidade." + Campo + ") + " + lb_aspas_duplas.Text + ", ";
                            gerarCampos += Campo;
                        }
                        gerarCampos = gerarCampos.Remove(gerarCampos.Length - 2);

                        richTextBox1.Text = "string query = @" + lb_aspas_duplas.Text + "Insert " + txt_tabela.Text + " (" + textBox1.Text + ")  values (" + gerarCampos + ")" + lb_aspas_duplas.Text + ";";
                    }
                }
            }

            else
            {
                if (Rb_Entidade.Checked == true)
                {
                    richTextBox1.Text = "";
                    foreach (string list in nomes)
                    {
                        Campo = list;
                        Campo = "        private string _" + Campo + ";\n        public string " + Campo + "\n        {\n           get { return _" + Campo + "; }\n           set { _" + Campo + " = value; }\n        }\n\n";
                        richTextBox1.Text += Campo;
                    }
                }

                if (rb_campos.Checked == true)
                {
                    richTextBox1.Text = "";
                    foreach (string list in nomes)
                    {
                        Campo = list;
                        Campo = "        Entidade." + Campo + " = " + Campo + ".Text;\n";
                        richTextBox1.Text += Campo;
                    }
                }
                
            }
        }

        private void rb_campos_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_campos.Checked == true)
            {
                rb_inverte.Visible = true;
            }
            else
            {
                rb_inverte.Visible = false;
            }
        }
        private void rb_inverte_Click(object sender, EventArgs e)
        {
            string[] nomes = textBox1.Text.Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            string Campo = "";

            if (rb_campos.Checked == true)
            {
                richTextBox1.Text = "";
                foreach (string list in nomes)
                {
                    Campo = list;
                    Campo = "        "+ Campo + ".Text = Entidade." + Campo + ";\n";
                    richTextBox1.Text += Campo;
                }
            }
        }

        private void rb_select_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_select.Checked == true)
            {
                lb_tabela.Visible = true;
                txt_tabela.Visible = true;
            }
            if (rb_select.Checked == false)
            {
                lb_tabela.Visible = false;
                txt_tabela.Visible = false;
            }
        }

        private void rb_Update_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_Update.Checked == true)
            {
                lb_tabela.Visible = true;
                txt_tabela.Visible = true;
            }
            if (rb_Update.Checked == false)
            {
                lb_tabela.Visible = false;
                txt_tabela.Visible = false;
            }
        }

        private void rb_insert_CheckedChanged(object sender, EventArgs e)
        {
            if (rb_insert.Checked == true)
            {
                lb_tabela.Visible = true;
                txt_tabela.Visible = true;
            }
            if (rb_insert.Checked == false)
            {
                lb_tabela.Visible = false;
                txt_tabela.Visible = false;
            }
        }
        
    }
}
